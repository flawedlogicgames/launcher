/**
 * 
 */
package flawedlogic.spacerace.launcher.update;


/**
 * @author Randy
 * represents something that the launcher can update
 */
public abstract class Updatable {
	protected Downloader downloader;

	protected String currentVersion;
	protected String newVersion;
	
	public Updatable() {
		
	}
	
	protected abstract void download();
	
	public void update() {
		if(compare() > 0) {
			System.out.println("downloading " + this.getClass());
			download();
		}
	}

	protected int compare() {
		System.out.println("Comparing \"" + currentVersion + "\" and \"" + newVersion + "\"");
		if(currentVersion.length() == 0) {
			System.out.println("old version number was not found");
			return 1;
		}
		if(newVersion.length() == 0) {
			System.out.println("new version number was not found");
			return 0;
		}
		String[] currentVersionParsed = currentVersion.split("\\.");
		String[] newVersionParsed = newVersion.split("\\.");
		for(int i = 0; i < currentVersionParsed.length; i++) {
			@SuppressWarnings("boxing")
			int comparison = new Integer(Integer.parseInt(currentVersionParsed[i])).compareTo(Integer.parseInt(newVersionParsed[i]));
			System.out.println(comparison);
			if(comparison != 0) {
				return comparison;
			}
		}
		System.out.println("identical");
		return 0;
	}
	
	/**
	 * @return the old version
	 */
	public String getCurrent() {
		return currentVersion;
	}

	/**
	 * @param currentVersion the current version to set
	 */
	public void setCurrent(String currentVersion) {
		this.currentVersion = currentVersion;
	}

	/**
	 * @return the new version
	 */
	public String getNew() {
		return newVersion;
	}

	/**
	 * @param newVersion the new version to set
	 */
	public void setNew(String newVersion) {
		this.newVersion = newVersion;
	}
}
