package flawedlogic.spacerace.launcher.update;

import flawedlogic.spacerace.launcher.Launcher;

/**
 * @author Randy
 *
 */
public class JogAmp extends Updatable {
	protected Downloader joglDownloader;
	protected Downloader joglNativesDownloader;
	protected Downloader gluegenDownloader;
	protected Downloader gluegenNativesDownloader;
	
	public JogAmp() {
		String folder = "http://jogamp.org/deployment/v" + newVersion + "/jar/";
		
		joglDownloader = new Downloader(folder + "jogl-all.jar", Launcher.get().getPaths().jogl());
		joglNativesDownloader = new Downloader(folder + "jogl-all-natives-windows-i586.jar", Launcher.get().getPaths().joglNatives());
		gluegenDownloader = new Downloader(folder + "gluegen-rt.jar", Launcher.get().getPaths().gluegen());
		gluegenNativesDownloader = new Downloader(folder + "gluegen-rt-natives-windows-i586.jar", Launcher.get().getPaths().gluegenNatives());
	}
	
	@Override
	protected void download() {
		joglDownloader.download();
		joglNativesDownloader.download();
		gluegenDownloader.download();
		gluegenNativesDownloader.download();
	}

	/**
	 * @param newVersion the new version to set
	 */
	@Override
	public void setNew(String newVersion) {
		this.newVersion = newVersion;
		
		String folder = "http://jogamp.org/deployment/v" + newVersion + "/jar/";
		
		joglDownloader = new Downloader(folder + "jogl-all.jar", Launcher.get().getPaths().jogl());
		joglNativesDownloader = new Downloader(folder + "jogl-all-natives-windows-i586.jar", Launcher.get().getPaths().joglNatives());
		gluegenDownloader = new Downloader(folder + "gluegen-rt.jar", Launcher.get().getPaths().gluegen());
		gluegenNativesDownloader = new Downloader(folder + "gluegen-rt-natives-windows-i586.jar", Launcher.get().getPaths().gluegenNatives());
	}
}
