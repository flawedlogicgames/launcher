package flawedlogic.spacerace.launcher.update;

import flawedlogic.spacerace.launcher.Launcher;

/**
 * @author Randy
 *
 */
public class Processing extends Updatable {
	
	public Processing() {
		downloader = new Downloader("https://bitbucket.org/flawedlogicgames/launcher/raw/master/core.jar", Launcher.get().getPaths().processing());
	}
	
	@Override
	protected void download() {
		downloader.download();
	}
}
