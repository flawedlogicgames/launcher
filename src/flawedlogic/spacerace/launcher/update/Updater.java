package flawedlogic.spacerace.launcher.update;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

import flawedlogic.spacerace.launcher.Launcher;

public class Updater {
	private Updatable spaceRace;
	private Updatable processing;
	private Updatable jogAmp;
	
	public Updater() {
		spaceRace = new SpaceRaceUpdate();
		processing = new Processing();
		jogAmp = new JogAmp();
	}
	
	public void update() {
		getCurrentVersions();
		getNewVersions();
		spaceRace.update();
		processing.update();
		jogAmp.update();
	}
	
	private void getCurrentVersions() {
		try(Scanner versionScanner = new Scanner(Launcher.get().getPaths().versions());) {
			System.out.println("getting version numbers");
			spaceRace.setCurrent(versionScanner.nextLine());
			processing.setCurrent(versionScanner.nextLine());
			jogAmp.setCurrent(versionScanner.nextLine());
		} catch(IOException e) {
			System.out.println("version file not found; will update all files");
			spaceRace.setCurrent("");
			processing.setCurrent("");
			jogAmp.setCurrent("");
		}
	}
	
	private void getNewVersions() {
		try(Scanner versionScanner = new Scanner(new URL("https://bitbucket.org/flawedlogicgames/launcher/raw/master/version").openStream());) {
			System.out.println("found latest version numbers");
			System.out.println("reading latest version numbers");
			spaceRace.setNew(versionScanner.nextLine());
			processing.setNew(versionScanner.nextLine());
			jogAmp.setNew(versionScanner.nextLine());
			System.out.println("read latest version numbers");
		} catch(IOException e) {
			e.printStackTrace();
			System.out.println("could not find latest version numbers; will not update any files");
			spaceRace.setNew("");
			processing.setNew("");
			jogAmp.setNew("");
		}
	}
}
