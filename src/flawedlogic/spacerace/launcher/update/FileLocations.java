package flawedlogic.spacerace.launcher.update;

import java.nio.file.Path;
import java.nio.file.Paths;

public enum FileLocations {
	WIN32(System.getenv("AppData"), "windows-i586"),
	WIN64(System.getenv("AppData"), "windows-amd64"),
	MAC("~/Library/Application Support/spacerace", "macosx-universal"),
	LINUX32("~/.spacerace", "linux-i586"),
	LINUX64("~/.spacerace", "linux-amd64");
	
	private Path directory;
	private Path lib;
	private Path SpaceRaceJar;
	private Path ProcessingJar;
	private Path joglJar;
	private Path joglNativesJar;
	private Path gluegenJar;
	private Path gluegenNativesJar;
	private Path versionsFile;
	
	private String nativesSuffix;
	
	FileLocations(String directory, String nativesSuffix) {
		this(Paths.get(directory), nativesSuffix);
	}
	
	FileLocations(Path parentDirectory, String nativesSuffix) {
		this.nativesSuffix = nativesSuffix;
		
		directory = parentDirectory.resolve("spacerace");
		lib = directory.resolve("lib");
		SpaceRaceJar = directory.resolve("spacerace.jar");
		ProcessingJar = lib.resolve("core.jar");
		joglJar = lib.resolve("jogl.jar");
		joglNativesJar = lib.resolve("jogl-natives-" + nativesSuffix + ".jar");
		gluegenJar = lib.resolve("gluegen.jar");
		gluegenNativesJar = lib.resolve("gluegen-natives-" + nativesSuffix + ".jar");
		versionsFile = directory.resolve("version");
	}

	/**
	 * @return the main directory
	 */
	public Path main() {
		return directory;
	}

	/**
	 * @return the lib director
	 */
	public Path lib() {
		return lib;
	}

	/**
	 * @return the space race jar path
	 */
	public Path spaceRace() {
		return SpaceRaceJar;
	}

	/**
	 * @return the processing jar path
	 */
	public Path processing() {
		return ProcessingJar;
	}

	/**
	 * @return the jogl jar path
	 */
	public Path jogl() {
		return joglJar;
	}

	/**
	 * @return the jogl-natives jar path
	 */
	public Path joglNatives() {
		return joglNativesJar;
	}

	/**
	 * @return the gluegen jar path
	 */
	public Path gluegen() {
		return gluegenJar;
	}

	/**
	 * @return the gluegen-natives jar path
	 */
	public Path gluegenNatives() {
		return gluegenNativesJar;
	}

	/**
	 * @return the versions file jar path
	 */
	public Path versions() {
		return versionsFile;
	}

	/**
	 * @return the natives suffix
	 */
	public String getNativesSuffix() {
		return nativesSuffix;
	}
}
