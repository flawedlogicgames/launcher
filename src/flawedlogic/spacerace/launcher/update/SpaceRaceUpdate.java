/**
 * 
 */
package flawedlogic.spacerace.launcher.update;

import flawedlogic.spacerace.launcher.Launcher;


/**
 * @author Randy
 *
 */
public class SpaceRaceUpdate extends Updatable {
	
	public SpaceRaceUpdate() {
		downloader = new Downloader("https://bitbucket.org/flawedlogicgames/launcher/downloads/spacerace.jar", Launcher.get().getPaths().spaceRace());
	}
	
	@Override
	protected void download() {
		downloader.download();
	}
}
