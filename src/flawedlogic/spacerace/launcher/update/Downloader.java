/**
 * 
 */
package flawedlogic.spacerace.launcher.update;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Randy
 *
 */
public class Downloader {
	private URL source;
	private Path destination;
	
	public Downloader(String source, String destination) {
		this(source, Paths.get(destination));
	}
	
	public Downloader(String source, Path destination) {
		try {
			this.source = new URL(source);
		} catch(MalformedURLException e) {
			e.printStackTrace();
		}
		this.destination = destination;
	}

	public void download() {
		System.out.println("downloading " + source.toString() + " to " + destination.toString());
		try {
			Files.createDirectories(destination.getParent());
			Files.createFile(destination);
		} catch(FileAlreadyExistsException e) {
			// File already exists
		} catch(IOException e) {
			e.printStackTrace();
		}
		try(InputStream is = source.openStream();
				ReadableByteChannel rbc = Channels.newChannel(is);
				FileOutputStream fos = new FileOutputStream(destination.toFile());
				FileChannel fc = fos.getChannel();
			) {
			
			fc.transferFrom(rbc, 0, Long.MAX_VALUE);
			System.out.println("finished downloading");
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
