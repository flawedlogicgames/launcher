package flawedlogic.spacerace.launcher;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import flawedlogic.spacerace.launcher.update.FileLocations;
import flawedlogic.spacerace.launcher.update.Updater;

public class Launcher extends JFrame {
	private static Launcher launcher;

	private FileLocations paths;
	Updater updater;
	
	public static void main(String[] args) {
		launcher = new Launcher();
		
		launcher.updater = new Updater();
		launcher.updater.update();
	}
	
	public Launcher() {
		super("Space Race Launcher");
		
		setLookAndFeel();
		addComponents();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		
		setPaths();
	}

	private void addComponents() {
		// TODO add GUI
	}
	
	private static void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(UnsupportedLookAndFeelException e) {
			e.printStackTrace();
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} catch(Exception e1) {
				e1.printStackTrace();
			}
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch(InstantiationException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the launcher
	 */
	public static Launcher get() {
		return launcher;
	}
	
	/**
	 * @return the fileLocations
	 */
	public FileLocations getPaths() {
		return paths;
	}
	
	private void setPaths() {
		String bitness = System.getProperty("sun.arch.data.model");
		String os = System.getProperty("os.name").toUpperCase();

		System.out.println(bitness);
		System.out.println(os);
		
		if(os.startsWith("WIN")) {
			if(bitness.equals("32")) {
				paths = FileLocations.WIN32;
			} else if(bitness.equals("64")) {
				paths = FileLocations.WIN64;
			}
		} else if(os.startsWith("MAC")) {
			paths = FileLocations.MAC;
		} else if(os.startsWith("LINUX")) {
			if(bitness.equals("32")) {
				paths = FileLocations.LINUX32;
			} else if(bitness.equals("64")) {
				paths = FileLocations.LINUX64;
			}
		}
		System.out.println(paths.name());
	}
}
